import React from "react";
import MyButton from "../Components/UI/button/MyButton";
import MyInput from "../Components/UI/input/MyInput";
import { useSetAuth } from "../context/context";

const Login = () => {
  const setIsAuth = useSetAuth();

  const login = (event) => {
    event.preventDefault();
    setIsAuth(true);
    localStorage.setItem("auth", "true");
  };

  return (
    <div>
      <h1 className="Log_h1">LogIn into Post World</h1>
      <form onSubmit={login}>
        <MyInput type="text" placeholder="Login" />
        <MyInput type="password" placeholder="Password" />
        <MyButton>LogIn</MyButton>
      </form>
    </div>
  );
};

export default Login;
