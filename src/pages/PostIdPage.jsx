import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import PostService from "../API/PostService";
import Loader from "../Components/UI/Loader/Loader";
import { useFetching } from "../hooks/useFetching";

const PostIdPage = () => {
  const params = useParams();

  const [post, setPost] = useState({});
  const [comment, setComment] = useState([]);
  const [fetchPostById, isLoadingPostById, postByIdError] = useFetching(
    async () => {
      const response = await PostService.getById(params.id);
      setPost(response.data);
    }
  );

  const [fetchComments, isLoadingComments, commentsIdError] = useFetching(
    async () => {
      const response = await PostService.getCommentsByPostId(params.id);
      setComment(response.data);
    }
  );

  useEffect(() => {
    fetchPostById();
    fetchComments();
  }, []);

  return (
    <div>
      <h1>Post Num: {params.id}</h1>
      <div>
        {post.id}. {post.title}
      </div>
      <div>{post.body}</div>
      <h3>Comments</h3>
      <div>
        {comment.map((comm) => (
          <div style={{ marginTop: "15px" }}>
            <h5>{comm.email}</h5>
            <div>{comm.body}</div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default PostIdPage;
