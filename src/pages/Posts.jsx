import React, { useEffect, useState } from "react";
import PostList from "../Components/PostList";
import "../Styles/App.css";
import PostForm from "../Components/PostForm";
import PostFilter from "../Components/PostFilter";
import MyModal from "../Components/UI/modal/MyModal";
import MyButton from "../Components/UI/button/MyButton";
import { usePosts } from "../hooks/usePosts";
import PostService from "../API/PostService";
import Loader from "../Components/UI/Loader/Loader";
import { useFetching } from "../hooks/useFetching";
import { getPageCount } from "../utils/pages";
import Pagination from "../Components/UI/pagination/Pagination";

function Posts() {
  const [posts, setPosts] = useState([]);

  const [filter, setFilter] = useState({ sort: "", query: "" });

  const [modal, setModal] = useState(false);

  const [totalCount, setTotalCount] = useState(0);

  const [limit, setLimit] = useState(10);

  const [page, setPage] = useState(1);

  const sortedAndSearchedPosts = usePosts(posts, filter.sort, filter.query);

  const [fetchPosts, isPostsLoading, postError] = useFetching(async () => {
    const response = await PostService.getAll(limit, page);
    setPosts([...posts, ...response.data]);
    const totalCount = response.headers["x-total-count"];
    setTotalCount(getPageCount(totalCount, limit));
  });

  const LoadMore = () => {
    if (page < totalCount) setPage(page + 1);
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    fetchPosts();
  }, [page]);

  const sortPosts = (sort) => {
    setFilter({ ...filter, sort: sort });
  };

  const createPost = (newPost) => {
    setPosts([...posts, newPost]);
    setModal(false);
  };

  const removePost = (post) => {
    setPosts(posts.filter((p) => p.id !== post.id));
  };

  const changePage = (p) => {
    setPage(p);
  };

  return (
    <div className="App">
      <MyButton style={{ marginTop: "20px" }} onClick={() => setModal(true)}>
        Add Post
      </MyButton>
      <MyModal visible={modal} setVisible={setModal}>
        <PostForm create={createPost} />
      </MyModal>

      <hr style={{ margin: "15px 0px" }} />

      <PostFilter filter={filter} setFilter={setFilter} sort={sortPosts} />

      {/* <Pagination totalCount={totalCount} page={page} changePage={changePage} /> */}

      {postError && <h1>Error accured ${postError}</h1>}
      <PostList
        remove={removePost}
        posts={sortedAndSearchedPosts}
        title="List of posts 1"
      />

      {isPostsLoading && (
        <div
          style={{
            display: "flex",
            marginTop: "100px",
            justifyContent: "center",
          }}
        >
          <Loader />{" "}
        </div>
      )}

      <div style={{ marginTop: "15px", marginLeft: "350px" }}>
        <MyButton onClick={LoadMore}>Load More</MyButton>
      </div>
      <div style={{ marginTop: "15px", marginLeft: "350px" }}>
        <MyButton onClick={scrollToTop}>Scroll Up</MyButton>
      </div>

      <Pagination totalCount={totalCount} page={page} changePage={changePage} />
    </div>
  );
}

export default Posts;
