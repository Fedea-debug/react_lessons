import { BrowserRouter } from "react-router-dom";
import AppRouter from "./Components/AppRouter";
import Navbar from "./Components/UI/Navbar/Navbar";
import "./Styles/App.css";
import { AuthProvider, useSetAuth } from "./context/context";
import { useEffect } from "react";
import { useState } from "react/cjs/react.development";

function App() {
  const [isLogin, setIsLogin] = useState();
  var login = {
    get: isLogin,
    set: setIsLogin,
  };

  const [isLoading, setIsLoading] = useState(true);
  var loading = {
    get: isLoading,
    set: setIsLoading,
  };

  useEffect(() => {
    if (localStorage.getItem("auth")) {
      login.set(true);
      console.log(localStorage.getItem("auth"));
    }
    loading.set(false);
  }, []);

  return (
    <AuthProvider login={login} loading={loading}>
      <BrowserRouter>
        <Navbar />
        <AppRouter />
      </BrowserRouter>
    </AuthProvider>
  );
}

export default App;
