import React from "react";
import { Link } from "react-router-dom";
import MyButton from "../button/MyButton";
import { useSetAuth } from "../../../context/context";

const Navbar = () => {
  const setIsAuth = useSetAuth();
  const logout = () => {
    setIsAuth(false);
    localStorage.removeItem("auth");
  };
  return (
    <div className="navbar">
      <MyButton onClick={logout}>LogOut</MyButton>
      <div className="navbarLinks">
        <Link to="/about">About</Link>
        <Link to="/posts">Posts</Link>
        <Link to="/login">Login</Link>
      </div>
    </div>
  );
};

export default Navbar;
