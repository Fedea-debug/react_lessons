import React from "react";
import MyInput from "./UI/input/MyInput";
import MySelect from "./UI/select/MySelect";

const PostFilter = ({ filter, setFilter, sort }) => {
  return (
    <div>
      <MyInput
        value={filter.query}
        onChange={(e) => setFilter({ ...filter, query: e.target.value })}
        type="text"
        placeholder="Search"
      />

      <MySelect
        value={filter.sort}
        onChange={sort}
        defaultValue="Sort"
        options={[
          { value: "title", name: "By Title" },
          { value: "body", name: "By Body" },
        ]}
      />
    </div>
  );
};
export default PostFilter;

// eslint-disable-next-line
{
  /* <MyInput
        value={filter.query}
        onChange={(e) => setFilter({ ...filter, query: e.target.value })}
        type="text"
        placeholder="Search"
      />

      <MySelect
        value={filter.sort}
        onChange={(selectedSort) => setFilter({ ...filter, sort: filter.sort })}
        defaultValue="Sort"
        options={[
          { value: "title", name: "By Title" },
          { value: "body", name: "By Body" },
        ]}
      /> */
}
