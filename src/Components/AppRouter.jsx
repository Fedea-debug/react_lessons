import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { publicRoutes, privateRoutes } from "../router/routes";
import { useAuth, useLoading } from "../context/context";
import Loader from "./UI/Loader/Loader";

const AppRouter = () => {
  const isAuth = useAuth();
  const isLoading = useLoading();

  if (isLoading) {
    return <Loader />;
  }

  return isAuth ? (
    <Switch>
      {privateRoutes.map((route) => (
        <Route
          path={route.path}
          exact={route.exact}
          component={route.component}
          key={route.path}
        />
      ))}
      <Redirect to="/posts" />
    </Switch>
  ) : (
    <Switch>
      {publicRoutes.map((route) => (
        <Route
          path={route.path}
          exact={route.exact}
          component={route.component}
          key={route.path}
        />
      ))}
      <Redirect to="/login" />
    </Switch>
  );
};

export default AppRouter;

{
  /* <Route path="/about">
<About />
</Route>

<Route exact path="/posts">
<Posts />
</Route>

<Route exact path="/posts/:id">
<PostIdPage />
</Route> */
}
