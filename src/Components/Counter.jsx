import React, { useState } from "react";

const Counter = function () {
  const [counter, setCounter] = useState(0);

  function Increment() {
    setCounter(counter + 1);
  }

  function Decrement() {
    setCounter(counter - 1);
  }

  function Reset() {
    setCounter(0);
  }

  return (
    <div>
      <h1>{counter}</h1>

      <button onClick={Increment}>Increment</button>

      <button onClick={Decrement}>Decrement</button>

      <button onClick={Reset}>Reset</button>
    </div>
  );
};
export default Counter;
