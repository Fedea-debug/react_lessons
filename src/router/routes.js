import About from "../pages/About";
import Posts from "../pages/Posts";
import PostsIdPage from "../pages/PostIdPage";
import Login from "../pages/Login";

export const privateRoutes = [
  { path: "/posts", exact: true, component: Posts },
  { path: "/about", exact: true, component: About },
  { path: "/posts/:id", exact: true, component: PostsIdPage },
];

export const publicRoutes = [{ path: "/login", exact: true, component: Login }];
