import React from "react";
import { useContext } from "react";

export const AuthContext = React.createContext(null);
export const setAuthContext = React.createContext(null);
export const LoadingContext = React.createContext(null);
export const setLoadingContext = React.createContext(null);

export function useAuth() {
  return useContext(AuthContext);
}

export function useSetAuth() {
  return useContext(setAuthContext);
}

export function useLoading() {
  return useContext(LoadingContext);
}
export function useSetLoading() {
  return useContext(setLoadingContext);
}

export function AuthProvider({ children, login, loading }) {
  return (
    <AuthContext.Provider value={login.get}>
      <setAuthContext.Provider value={login.set}>
        <LoadingContext.Provider value={loading.get}>
          <setLoadingContext.Provider value={loading.set}>
            {children}
          </setLoadingContext.Provider>
        </LoadingContext.Provider>
      </setAuthContext.Provider>
    </AuthContext.Provider>
  );
}
